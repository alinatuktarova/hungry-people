"use strict";

var menu = document.querySelector('.menu');
var main = document.querySelector('main');
var right_menu = document.querySelector('.right_menu').innerHTML;
var left_menu = document.querySelector('.left_menu').innerHTML;
var count = 0;

function changeMenu() {
  if (count == 0) {
    var small_menu = document.createElement('div');
    small_menu.setAttribute('class', 'small-menu');
    var small_list = document.createElement('ul');
    small_list.setAttribute('class', 'small-menu__item');
    small_menu.appendChild(small_list);
    small_list.innerHTML = left_menu + right_menu;
    menu.style.backgroundImage = 'url("./images/cancel.png")';
    document.querySelector('.imgVector').style.display = "none";
    main.innerHTML += small_menu.outerHTML;
    count = 1;
  } else {
    document.querySelector('.small-menu').remove();
    menu.style.backgroundImage = 'url("./images/menu.png")';
    document.querySelector('.imgVector').style.display = "block";
    count = 0;
  }
}